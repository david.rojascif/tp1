//
// Created by david on 01/03/2022.
//

#ifndef TP1_CALCULATOR_HPP
#define TP1_CALCULATOR_HPP
#include <string>
#include <bits/stdc++.h>
#define END_APP '#'
#define CHAR_ASCII 48
class Calculator{
        public:
        Calculator();
        void askUserInput(char& input);
        double getCalculationResult();
        char* getCalculationAsString();
        char getOperation();
        bool isThereError();
        bool isAppFinished();
        private:
        bool is_digits(const std::string &str);
        double setFirstOperand(double firstOperand);
        double setSecondOperand(double secondOperand);
        std::string getOperandAsString(double operand);
        std::string getOperationAsString();
        enum errorTypes {NO_PROBLEMS, DIVISION_BY_ZERO, NOT_AN_OPERATION, NOT_A_NUMBER};
        enum calculationState {GET_USER_NUMBER_INPUT, GET_USER_OPERAND_INPUT, OUTPUT_READY};
        struct operations{
            static const char PLUS='+';
            static const char MINUS='-';
            static const char MULTIPLY='*';
            static const char DIVIDE='/';
        };
        errorTypes errorFound_;
        calculationState currentState_;
        char operation_;
        double firstOperand_;
        double secondOperand_;
        char calcString[100];
};

#endif //TP1_CALCULATOR_HPP

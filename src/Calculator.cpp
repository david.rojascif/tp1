//
// Created by david on 01/03/2022.
//

#include "../include/Calculator.hpp"


Calculator::Calculator(): errorFound_(errorTypes::NO_PROBLEMS), currentState_(calculationState::GET_USER_NUMBER_INPUT),
                          operation_(operations::PLUS), firstOperand_(0), secondOperand_(0){

}

void Calculator::askUserInput(char &input) {
    scanf("%s",&input);
    switch (currentState_) {
        case GET_USER_NUMBER_INPUT:
            if (is_digits(std::to_string(input))){
                if (!firstOperand_){
                    setFirstOperand((double)input-CHAR_ASCII);
                    currentState_=calculationState::GET_USER_OPERAND_INPUT;
                } else{
                    setSecondOperand((double)input-CHAR_ASCII);
                    currentState_=calculationState::OUTPUT_READY;
                }
                errorFound_=errorTypes::NO_PROBLEMS;
            } else{
                errorFound_=errorTypes::NOT_A_NUMBER;
            }
            break;
        case GET_USER_OPERAND_INPUT:
            switch (input) {
                case operations::PLUS :
                    operation_=input;
                    errorFound_=errorTypes::NO_PROBLEMS;
                    break;
                case operations::MINUS :
                    operation_=input;
                    errorFound_=errorTypes::NO_PROBLEMS;
                    break;
                case operations::MULTIPLY :
                    operation_=input;
                    errorFound_=errorTypes::NO_PROBLEMS;
                    break;
                case operations::DIVIDE :
                    operation_=input;
                    errorFound_=errorTypes::NO_PROBLEMS;
                    break;
                case END_APP:
                    operation_=END_APP;
                    errorFound_=errorTypes::NO_PROBLEMS;
                    break;
                default:
                    operation_=END_APP;
                    errorFound_=errorTypes::NOT_AN_OPERATION;
            }
            currentState_=calculationState::GET_USER_NUMBER_INPUT;
            break;
        case OUTPUT_READY:
            errorFound_=errorTypes::NO_PROBLEMS;
            currentState_=calculationState::GET_USER_NUMBER_INPUT;
            askUserInput(input);
            break;
    }
}

double Calculator::getCalculationResult() {
    switch (operation_) {
        case operations::PLUS :
            return firstOperand_+secondOperand_;
        case operations::MINUS :
            return firstOperand_-secondOperand_;
        case operations::MULTIPLY :
            return firstOperand_*secondOperand_;
        case operations::DIVIDE :
            if (secondOperand_==0){
                errorFound_=errorTypes::DIVISION_BY_ZERO;
                return 0;
            } else{
                return firstOperand_/secondOperand_;
            }
    }
    errorFound_=errorTypes::NO_PROBLEMS;
    return 0;
}

char* Calculator::getCalculationAsString() {
    if ((isThereError()||currentState_!=OUTPUT_READY)||isAppFinished()){
        strcpy(calcString, "");
        return calcString;
    }
    std::string calculation;
    std::string firstOP= getOperandAsString(firstOperand_);
    std::string secondOP= getOperandAsString(secondOperand_);
    calculation= firstOP
            .append(" ")
            .append(getOperationAsString())
            .append(" ")
            .append(secondOP)
            .append(" ")
            .append("=")
            .append(" ")
            .append(getOperandAsString(getCalculationResult()));
    strcpy(calcString, calculation.c_str());
    return calcString;
}

char Calculator::getOperation() {
    return operation_;
}

double Calculator::setFirstOperand(double firstOperand) {
    if (!isThereError())firstOperand_=firstOperand;
    return firstOperand_;
}

double Calculator::setSecondOperand(double secondOperand) {
    if (!isThereError())secondOperand_=secondOperand;
    return secondOperand_;
}

bool Calculator::isThereError() {
    switch (errorFound_) {
        case NO_PROBLEMS:
            return false;
        case DIVISION_BY_ZERO:
            return true;
        case NOT_AN_OPERATION:
            return true;
        case NOT_A_NUMBER:
            return true;
    }
    return true;
}

bool Calculator::is_digits(const std::string &str){
    return std::all_of(str.begin(), str.end(), ::isdigit);
}

bool Calculator::isAppFinished() {
    return getOperation()==END_APP;
}

std::string Calculator::getOperandAsString(double operand) {
    std::string str = std::to_string (operand);
    str.erase ( str.find_last_not_of('0') + 1, std::string::npos );
    return str;
}

std::string Calculator::getOperationAsString() {
    return std::to_string((char)getOperation());
}


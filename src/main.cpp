//
// Created by david on 01/03/2022.
//
#include <Calculator.hpp>
int main(){
    char userInput;
    Calculator calculator= Calculator();
    while (!calculator.isAppFinished()){
        calculator.askUserInput((char &) userInput);
        printf(calculator.getCalculationAsString());
    }
    return 0;
}